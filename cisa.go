package main

import (
	"encoding/json"
	"io"
	"net/http"
)

func FetchKEVs() (map[string]bool, error) {
	resp, err := http.Get("https://www.cisa.gov/sites/default/files/feeds/known_exploited_vulnerabilities.json")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var kves KVEs
	err = json.Unmarshal(body, &kves)
	if err != nil {
		return nil, err
	}

	result := make(map[string]bool)
	for _, vuln := range kves.Vulnerabilities {
		result[vuln.CveID] = true
	}

	return result, nil
}

type KVEs struct {
	Vulnerabilities []struct {
		CveID string `json:"cveID"`
	} `json:"vulnerabilities"`
}
