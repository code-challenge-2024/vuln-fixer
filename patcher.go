package main

import (
	"bytes"
	"fmt"
	"path/filepath"
	"regexp"
	"strings"
)

type Patcher struct {
	glCli *GitLabCli
	kevs  map[string]bool
}

func NewPatcher(glCli *GitLabCli, kevs map[string]bool) *Patcher {
	return &Patcher{
		glCli: glCli,
		kevs:  kevs,
	}
}

func (p *Patcher) PatchProject(projectID string) error {
	// load GitLab vulns
	vulns, err := p.glCli.FetchDependencyVulnerabilities(projectID)
	if err != nil {
		panic(err)
	}

	// cross reference GitLab vulns with KEVs
	updates := make(map[string]map[string]string)
	for _, vuln := range vulns {
		if !p.kevs[vuln.CVE] {
			continue
		}

		pkgs, ok := updates[vuln.File]
		if !ok {
			pkgs = make(map[string]string)
			updates[vuln.File] = pkgs
		}
		pkgs[vuln.Package] = ""
	}

	// fetch latest package versions
	for file, pkgs := range updates {
		for pkg := range pkgs {
			latest, err := FetchLatestVersion(file, pkg)
			if err != nil {
				return fmt.Errorf("PatchProject: %w", err)
			}
			pkgs[pkg] = latest
		}
	}

	// patch the vulns
	for file, pkgs := range updates {
		err := p.patchPackages(projectID, file, pkgs)
		if err != nil {
			return fmt.Errorf("PatchProject: %w", err)
		}
	}

	return nil
}

func (p *Patcher) patchPackages(projectID string, file string, updates map[string]string) error {
	if strings.HasSuffix(file, "package-lock.json") {
		file = strings.TrimSuffix(file, "package-lock.json")
		file += "package.json"
	}

	data, err := p.glCli.FetchRawFile(projectID, file)
	if err != nil {
		return fmt.Errorf("patchVuln: %w", err)
	}

	var newData []byte
	switch filepath.Base(file) {
	case "pom.xml":
		newData = patchMaven(data, updates)
	case "package.json":
		newData = patchNPM(data, updates)
	default:
		return fmt.Errorf("patchPackages: unsupported file: %s", file)
	}

	if bytes.Equal(data, newData) {
		// no op
		return nil
	}

	err = p.glCli.PushFix(
		projectID,
		file,
		string(newData),
		"DJ Poncho",
		"sytse@gitlab.com",
		"Fix critical vulnerability",
	)
	if err != nil {
		return err
	}

	return nil
}

func patchMaven(data []byte, updates map[string]string) []byte {
	result := data

	for pkg, latest := range updates {
		groupID, _, _ := strings.Cut(pkg, "/")

		result = regexp.MustCompile(
			fmt.Sprintf(
				`(?sU)<groupId>(\s*%s\s*)</groupId>(\s*)<artifactId>(\s*.*\s*)</artifactId>(\s*)<version>.*</version>`,
				regexp.QuoteMeta(groupID),
			),
		).ReplaceAll(
			result,
			[]byte(
				fmt.Sprintf(
					`<groupId>$1</groupId>$2<artifactId>$3</artifactId>$4<version>%s</version>`,
					latest,
				),
			),
		)
	}

	return result
}

func patchNPM(data []byte, updates map[string]string) []byte {
	return data
}
