## Vuln Fixer

### Usage

This tool expects a GitLab API token (`api` scope) and a list of one or more groups to patch vulnerabilities in.

```sh
GITLAB_TOKEN="..." go run *.go "code-challenge-2024" "gitlab-org/incubation-engineering/code-challenge-2024"
```

### How it works

This tool interacts with GitLab exclusively using the REST APIs.

1. For each group provided as input, list all of its projects
1. Fetches all dependency scanning vulnerabilities of the project
1. Loads the Known Exploited Vulnerabilities list from CISA
1. Cross references projects vulnerabilities with KEVs, keeping criticial vulnerabilities only
1. Fetches the latest available version for each vulnerable package from deps.dev
1. Fetches the package manager file (`pom.xml`, `package.json`, etc.) from the repository
1. Patches the package manager file using regexes
1. Pushes the fix to a new random branch
1. Opens an MR that will auto-merge upon pipeline success
