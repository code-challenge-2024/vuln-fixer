package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/xanzy/go-gitlab"
)

type GitLabCli struct {
	cli *gitlab.Client
}

func NewGitLabCli(token string) (*GitLabCli, error) {
	cli, err := gitlab.NewClient(token)
	if err != nil {
		return nil, fmt.Errorf("NewGitLabCli: %w", err)
	}

	return &GitLabCli{
		cli: cli,
	}, nil
}

func (g *GitLabCli) ListGroupProjects(group string) ([]*gitlab.Project, error) {
	trueRef := true

	projects, _, err := g.cli.Groups.ListGroupProjects(group, &gitlab.ListGroupProjectsOptions{
		IncludeSubGroups: &trueRef,
	})
	if err != nil {
		return nil, fmt.Errorf("ListGroupProjects: %w", err)
	}

	return projects, nil
}

func (g *GitLabCli) FetchDependencyVulnerabilities(projectID string) ([]DependencyVulnerability, error) {
	vulns, _, err := g.cli.ProjectVulnerabilities.ListProjectVulnerabilities(projectID, nil)
	if err != nil {
		return nil, fmt.Errorf("FetchDependencyVulnerabilities list: %w", err)
	}

	var result []DependencyVulnerability

	for _, vuln := range vulns {
		if vuln.ReportType != "dependency_scanning" {
			continue
		}

		var raw RawMetadata
		err = json.Unmarshal([]byte(vuln.Finding.RawMetadata), &raw)
		if err != nil {
			return nil, fmt.Errorf("FetchDependencyVulnerabilities unmarshal: %w", err)
		}

		cve := ""
		for _, identifier := range raw.Identifiers {
			if identifier.Type == "cve" {
				cve = identifier.Name
			}
		}

		result = append(result, DependencyVulnerability{
			File:    raw.Location.File,
			CVE:     cve,
			Package: raw.Location.Dependency.Package.Name,
		})
	}

	return result, err
}

type DependencyVulnerability struct {
	File    string
	CVE     string
	Package string
}

type RawMetadata struct {
	Location struct {
		File       string `json:"file"`
		Dependency struct {
			Package struct {
				Name string `json:"name"`
			} `json:"package"`
			Version string `json:"version"`
		} `json:"dependency"`
	} `json:"location"`
	Identifiers []struct {
		Type string `json:"type"`
		Name string `json:"name"`
	} `json:"identifiers"`
}

func (g *GitLabCli) FetchRawFile(projectID string, file string) ([]byte, error) {
	data, _, err := g.cli.RepositoryFiles.GetRawFile(projectID, file, nil, nil)
	if err != nil {
		return nil, fmt.Errorf("FetchRawFile: %w", err)
	}

	return data, nil
}

func (g *GitLabCli) PushFix(projectID string, file, content, authorName, authorEmail, commitMsg string) error {
	// Push fix to new branch
	main := "main"
	branch := fmt.Sprintf("security-fix-%s", uuid.NewString())

	_, _, err := g.cli.RepositoryFiles.UpdateFile(projectID, file, &gitlab.UpdateFileOptions{
		Branch:        &branch,
		Content:       &content,
		AuthorName:    &authorName,
		AuthorEmail:   &authorEmail,
		CommitMessage: &commitMsg,
		StartBranch:   &main,
	}, nil)

	if err != nil {
		return fmt.Errorf("PushFile update file: %w", err)
	}

	// Create MR
	title := "Fix critical vulnerability"
	description := ""

	mr, _, err := g.cli.MergeRequests.CreateMergeRequest(projectID, &gitlab.CreateMergeRequestOptions{
		Title:        &title,
		Description:  &description,
		SourceBranch: &branch,
		TargetBranch: &main,
	}, nil)

	if err != nil {
		return fmt.Errorf("PushFile create MR: %w", err)
	}

	// Set auto-merge
	mergeAfterPipeline := true
	removeSource := true

	for i := 0; i < 10; i++ {
		_, _, err = g.cli.MergeRequests.AcceptMergeRequest(projectID, mr.IID, &gitlab.AcceptMergeRequestOptions{
			MergeWhenPipelineSucceeds: &mergeAfterPipeline,
			ShouldRemoveSourceBranch:  &removeSource,
		}, nil)

		if err == nil {
			break
		}

		time.Sleep(500 * time.Millisecond)
	}

	if err != nil {
		return fmt.Errorf("PushFile accept MR: %w", err)
	}

	return nil
}
