package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		panic("expecting at least one group ID or name")
	}

	// init GitLab cli
	token := os.Getenv("GITLAB_TOKEN")
	if token == "" {
		panic("missing GITLAB_TOKEN env var")
	}
	glCli, err := NewGitLabCli(token)
	if err != nil {
		panic(err)
	}

	// fetch KEVs
	kevs, err := FetchKEVs()
	if err != nil {
		panic(err)
	}

	patcher := NewPatcher(glCli, kevs)

	for _, group := range os.Args[1:] {
		fmt.Println("group:", group)
		projects, err := glCli.ListGroupProjects(group)
		if err != nil {
			panic(err)
		}

		for _, project := range projects {
			if err := patcher.PatchProject(fmt.Sprintf("%d", project.ID)); err == nil {
				fmt.Println("", "project:", project.WebURL, "patched ✅")
			} else {
				fmt.Println("", "project:", project.WebURL, "failed ❌", err)
			}
		}
	}
}
