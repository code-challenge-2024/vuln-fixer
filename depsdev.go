package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"

	"golang.org/x/mod/semver"
)

func FetchLatestVersion(file string, pkg string) (string, error) {
	manager := ""

	switch filepath.Base(file) {
	case "pom.xml":
		manager = "MAVEN"
		pkg = strings.ReplaceAll(pkg, "/", ":")
	case "package-lock.json":
		manager = "NPM"
	default:
		return "", fmt.Errorf("unsupported package manager: %s", file)
	}

	u := fmt.Sprintf("https://api.deps.dev/v3/systems/%s/packages/%s", manager, url.PathEscape(pkg))
	resp, err := http.Get(u)
	if err != nil {
		return "", fmt.Errorf("FetchLatestVersion get: %w", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("FetchLatestVersion read: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("FetchLatestVersion status code: %d %s", resp.StatusCode, u)
	}

	var respObj struct {
		Versions []struct {
			VersionKey struct {
				Version string `json:"version"`
			} `json:"versionKey"`
		} `json:"versions"`
	}

	err = json.Unmarshal(body, &respObj)
	if err != nil {
		return "", fmt.Errorf("FetchLatestVersion unmarshal: %w", err)
	}

	var versions []string
	for _, version := range respObj.Versions {
		v := version.VersionKey.Version
		if !strings.Contains(v, "-") {
			if !strings.HasPrefix(v, "v") {
				v = "v" + v
			}
			versions = append(versions, v)
		}
	}

	if len(versions) == 0 {
		return "", fmt.Errorf("FetchLatestVersion no versions for: %s", u)
	}

	semver.Sort(versions)

	latest := versions[len(versions)-1]
	latest = strings.TrimPrefix(latest, "v")

	return latest, nil
}
